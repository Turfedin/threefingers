import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:threefingers/fingers.dart';
import 'package:threefingers/settings.dart' as TF;

class PassiveFingers extends StatelessWidget {
  PassiveFingers({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Flexible(
          flex: 7,
          child: Row(
            children: [
              Expanded(
                child: PassiveFinger(FingersType.agree),
              ),
              Expanded(
                child: PassiveFinger(FingersType.disagree),
              ),
            ],
          ),
        ),
        Flexible(
          flex: 3,
          child: Row(
            children: [
              Expanded(
                child: PassiveFinger(FingersType.enough),
              ),
              Expanded(
                child: PassiveFinger(FingersType.pan),
              ),
              Expanded(
                child: PassiveFinger(FingersType.cannot_hear),
              ),
              Expanded(
                child: PassiveFinger(FingersType.more_quiet),
              ),
              Expanded(
                child: PassiveFinger(FingersType.louder),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

// ignore: slash_for_doc_comments
/**** TASK14 ****/
class PassiveFinger extends StatefulWidget {
  final Finger finger;

  PassiveFinger(FingersType type, {Key? key})
      : finger = Finger(type),
        super(key: key);

  // No need to memorize the FingersType here since we can get it from finger
  FingersType get type {
    return finger.type;
  }

  @override
  _PassiveFingerState createState() => _PassiveFingerState();
}

class _PassiveFingerState extends State<PassiveFinger> with WidgetsBindingObserver {
  bool active = false;
  late AsyncSnapshot<DocumentSnapshot> documentSnapshot;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState newAppState) {
    setState(() {
      // If the app is not being used, the passive fingers are deactivated and
      // not counted;
      if (newAppState != AppLifecycleState.resumed && newAppState != AppLifecycleState.inactive) {
        if (active) {
          active = false;
          documentSnapshot.data?.reference.update({'count': FieldValue.increment(-1)});
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    DocumentReference session = Provider.of<TF.Settings>(context).session;
    bool isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;

    return StreamBuilder<DocumentSnapshot>(
      stream: session.collection('passiveFingers').doc(widget.type.asString()).snapshots(),
      builder: (context, snapshot) {
        documentSnapshot = snapshot;
        int? count = 0;
        try {
          count = snapshot.data!['count'];
        } catch (e) {
          count = -1;
        }

        // To help correct potential inconsitencies
        if (count! <= 0 && active) {
          // Cannot setState during build, so we schedule it for right after
          WidgetsBinding.instance!.addPostFrameCallback((_) {
            setState(() {
              active = false;
            });
          });
        }

        return GestureDetector(
          onTap: () {
            // No need to setState since updating the Stream already rebuilds.
            active = !active;
            if (active)
              snapshot.data!.reference.update({'count': FieldValue.increment(1)});
            else {
              if (count! > 0) snapshot.data!.reference.update({'count': FieldValue.increment(-1)});
            }
          },
          child: Container(
            padding: EdgeInsets.all(6),
            margin: EdgeInsets.all(2),
            decoration: BoxDecoration(
              border: Border.all(
                color: active ? Theme.of(context).primaryColorDark : Theme.of(context).primaryColor,
                width: 2,
              ),
              borderRadius: BorderRadius.all(Radius.circular(8)),
              color: active ? Theme.of(context).primaryColor : Colors.transparent,
            ),
            child: Flex(
              direction: isPortrait ? Axis.vertical : Axis.horizontal,
              children: [
                Flexible(
                  flex: isPortrait ? 2 : 1,
                  child: Align(
                    alignment: Alignment.center,
                    child: widget.finger.image,
                  ),
                ),
                if (count != 0)
                  Flexible(
                    flex: 1,
                    fit: FlexFit.tight,
                    child: FittedBox(
                      fit: isPortrait ? BoxFit.fitWidth : BoxFit.fitHeight,
                      child: count == -1
                          ? Padding(
                              padding: EdgeInsets.all(8),
                              child: CircularProgressIndicator(
                                strokeWidth: 2,
                              ),
                            )
                          : Text(
                              '$count',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: active
                                    ? Theme.of(context).primaryColorDark
                                    : Theme.of(context).primaryColor,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                    ),
                  ),
              ],
            ),
          ),
        );
      },
    );
  }
}
