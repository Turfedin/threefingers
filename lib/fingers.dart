import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:threefingers/dialog.dart';

enum FingersType {
  agree,
  bullshit,
  cannot_hear,
  clarification,
  comment,
  disagree,
  enough,
  louder,
  more_quiet,
  pan,
  question,
  technical,
}

// Allows a comprehensive way to change FingersType variables to Strings
// corresponding to the Firecode documents. The default toString() returns
// 'FingersType.agree' for example, but we only want 'agree'.
extension FieldName on FingersType {
  String asString() => this.toString().split('.').last;
}

// ignore: slash_for_doc_comments
/**** TASK8 & TASK9 ****/
final Map<FingersType, String> assetsPath = {
  FingersType.agree: 'assets/agree.svg',
  FingersType.bullshit: 'assets/bullshit.svg',
  FingersType.cannot_hear: 'assets/cannot_hear.svg',
  FingersType.clarification: 'assets/clarification.svg',
  FingersType.comment: 'assets/comment.svg',
  FingersType.disagree: 'assets/disagree.svg',
  FingersType.enough: 'assets/enough.svg',
  FingersType.louder: 'assets/louder.svg',
  FingersType.more_quiet: 'assets/more_quiet.svg',
  FingersType.pan: 'assets/pan.svg',
  FingersType.question: 'assets/question.svg',
  FingersType.technical: 'assets/technical.svg',
};

final Map<String, FingersType> typeFromName = {
  'agree': FingersType.agree,
  'bullshit': FingersType.bullshit,
  'cannot_hear': FingersType.cannot_hear,
  'clarification': FingersType.clarification,
  'comment': FingersType.comment,
  'disagree': FingersType.disagree,
  'enough': FingersType.enough,
  'louder': FingersType.louder,
  'more_quiet': FingersType.more_quiet,
  'pan': FingersType.pan,
  'question': FingersType.question,
  'technical': FingersType.technical,
};

// All fingers with a strictly positive priority should have a different
// priority value.
// ignore: slash_for_doc_comments
/**** TASK10 ****/
final Map<FingersType, int> fingerPriorities = {
  FingersType.agree: -1,
  FingersType.bullshit: 1,
  FingersType.cannot_hear: -1,
  FingersType.clarification: 3,
  FingersType.comment: 4,
  FingersType.disagree: -1,
  FingersType.enough: -1,
  FingersType.louder: -1,
  FingersType.more_quiet: -1,
  FingersType.pan: -1,
  FingersType.question: 10,
  FingersType.technical: 2,
};

class Finger implements Comparable {
  // Easy to use with Firestone cloud
  final Map<String, dynamic> fingerData;
  FingersType type;
  String? comment;
  final Widget image;

  Finger(this.type, {String? owner, String? id})
      : fingerData = Map<String, dynamic>(),
        image = FingerImage(assetsPath[type]!) {
    fingerData['type'] = type.asString();
    fingerData['priority'] = fingerPriorities[type];
    fingerData['owner'] = owner ?? 'Anonymous';
    fingerData['ownerId'] = id ?? '';
    // Time without microseconds
    fingerData['creationTime'] = DateTime.now().toIso8601String().substring(0, 23);
    fingerData['isSpeaker'] = false;
  }

  Finger.fromMap(this.fingerData)
      : type = typeFromName[fingerData['type']]!,
        image = FingerImage(assetsPath[typeFromName[fingerData['type']]!]!);

  int get priority => fingerData['priority'];

  String get owner => fingerData['owner'];

  String get ownerId => fingerData['ownerId'];

  DateTime get creationTime => DateTime.parse(fingerData['creationTime']);

  bool get isSpeaker => fingerData['isSpeaker'];

  set isSpeaker(bool updated) => fingerData['isSpeaker'] = updated;

  @override
  int compareTo(dynamic other) {
    if (priority != other.priority) return priority - other.priority as int;
    int dateComparison = creationTime.compareTo(other.creationTime);
    if (dateComparison != 0) return dateComparison;
    return ownerId.compareTo(other.ownerId);
  }

  void edit(BuildContext callingContext) async {
    var controller = TextEditingController(text: comment);
    controller.addListener(() {
      _saveTextFromEdit(controller);
    });

    await showDialog(
      context: callingContext,
      builder: (callingContext) =>
          EditingDialog(controller, 'Comment your finger', 'A beautiful comment...', finger: this),
    );
  }

  void _saveTextFromEdit(TextEditingController controller) {
    comment = controller.text;
  }
}

// ignore: slash_for_doc_comments
/**** TASK9 ****/
class FingerImage extends StatelessWidget {
  final String path;

  FingerImage(this.path, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      path,
      semanticsLabel: 'A finger',
      fit: BoxFit.contain,
    );
  }
}
