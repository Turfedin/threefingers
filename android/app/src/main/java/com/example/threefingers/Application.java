package io.flutter.plugins.threefingers;

import io.flutter.app.FlutterApplication;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugins.GeneratedPluginRegistrant;
import io.flutter.embedding.engine.FlutterEngine;

public class Application extends FlutterApplication{

  public void registerWith(PluginRegistry registry) {
    GeneratedPluginRegistrant.registerWith((FlutterEngine) registry);
  }
}
