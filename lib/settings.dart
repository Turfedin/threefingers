import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:path_provider/path_provider.dart';

import 'package:threefingers/dialog.dart';

class Settings extends ChangeNotifier {
  File? appFile;
  String? user;
  String? userToken;
  FirebaseMessaging messaging;
  late DocumentReference session;
  bool? isFacilitator;
  bool? isDarkTheme;
  ThemeMode themeMode = ThemeMode.system;

  Settings() : messaging = FirebaseMessaging.instance {
    // For all plateforms except web, might need further configuration
    messaging.requestPermission();

    _fetchId();
    _loadFromSaved();
  }

  set theme(bool isDark) {
    if (isDarkTheme == isDark) return;
    isDarkTheme = isDark;

    if (isDarkTheme!)
      themeMode = ThemeMode.dark;
    else
      themeMode = ThemeMode.light;

    notifyListeners();
  }

  Future<void> updateFacilitator() async {
    var snapShot = await session.get();
    String? facilitator;
    try {
      facilitator = snapShot.get('facilitator');
    } catch (e) {
      // It is likely that the session does not exist anymore
      isFacilitator = false;
      return;
    }

    if (isFacilitator == null) {
      isFacilitator = (facilitator == userToken);
      // We do not call notifyListeners() because we assume we are in the
      // initialization phase and it will be called later.
    } else if (isFacilitator!) {
      isFacilitator = false;
      notifyListeners();
      await FirebaseFirestore.instance
          .runTransaction((transaction) async => transaction.set(session, {'facilitator': ''}));
    } else {
      if (facilitator == '') {
        isFacilitator = true;
        notifyListeners();
        await FirebaseFirestore.instance.runTransaction(
            (transaction) async => transaction.set(session, {'facilitator': userToken}));
      }
    }
  }

  void _fetchId() async {
    userToken = await messaging.getToken(
      vapidKey:
          "BNOo9Z4gu3-2qrGOIqiP1KcWT8Gdq1U7VOaMq4YOXYMmnUcQbWnWej5QJoCO80z7uIkEaKMKfPqEoHrFzAPKQLE",
    );
  }

  Future<String?> fetchId() async => userToken;

  void _loadFromSaved() async {
    Map<String, dynamic>? savedData;
    Directory directory;

    try {
      // Not supported on web as of the 28/03/2021
      directory = await getApplicationDocumentsDirectory();
      appFile = File(directory.path + '/settings.json');

      String jsonString = await appFile!.readAsString();
      savedData = jsonDecode(jsonString);
    } catch (e) {
      savedData = null;
    }

    if (savedData == null || savedData.length == 0) {
      user = 'Anonymous';
      isDarkTheme = null;
    } else {
      user = savedData['username'];
      // Potentially null, and that's ok. It means that system default is used.
      isDarkTheme = savedData['dark'];
    }
  }

  void save() {
    var dataToSave = <String, dynamic>{'username': user};
    if (isDarkTheme != null) dataToSave['dark'] = isDarkTheme;

    String jsonString = jsonEncode(dataToSave);
    try {
      appFile?.writeAsString(jsonString);
    } catch (e) {}
  }

  Future<void> initSession(BuildContext context) async {
    String? sessionPath = await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return SessionDialog();
        });
    session = FirebaseFirestore.instance.collection('sessions').doc(sessionPath);

    var snapShot = await FirebaseFirestore.instance.collection('sessions').doc(sessionPath).get();
    if (!snapShot.exists)
      await _createSession();
    else {
      isFacilitator = null;
      await updateFacilitator();
    }

    notifyListeners();
  }

  Future<void> _createSession() async {
    // The creator of a session is its facilitator by default
    isFacilitator = true;

    await FirebaseFirestore.instance.runTransaction((transaction) async =>
        // Set the creator of the session as its facilitator
        transaction.set(session, {'facilitator': userToken}));

    await resetCounts();
  }

  Future<void> resetCounts() async {
    List<String> fingerNames = [
      'agree',
      'cannot_hear',
      'disagree',
      'enough',
      'louder',
      'more_quiet',
      'pan'
    ];

    for (String name in fingerNames) {
      await FirebaseFirestore.instance.runTransaction((transaction) async =>
          transaction.set(session.collection('passiveFingers').doc(name), {'count': 0}));
    }
  }
}

// ignore: slash_for_doc_comments
/**** TASK1 ****/
final ThemeData darkTheme = ThemeData(
  brightness: Brightness.dark,
  primaryColor: Colors.cyan,
  primaryColorDark: Colors.indigo,
  visualDensity: VisualDensity.adaptivePlatformDensity,
);

final ThemeData lightTheme = ThemeData(
  brightness: Brightness.light,
  primaryColor: Colors.cyan,
  primaryColorDark: Colors.indigo,
  visualDensity: VisualDensity.adaptivePlatformDensity,
);
