import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:threefingers/fingers.dart';
import 'package:threefingers/fingersqueue.dart';
import 'package:threefingers/settings.dart' as TF;

class ActiveFingers extends StatelessWidget {
  final bool isListDeployed;

  ActiveFingers(this.isListDeployed, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int tilesInRow = MediaQuery.of(context).orientation == Orientation.portrait ? 2 : 4;
    tilesInRow -= isListDeployed ? 1 : 0;

    if (Theme.of(context).platform != TargetPlatform.iOS &&
        Theme.of(context).platform != TargetPlatform.android) {
      tilesInRow = MediaQuery.of(context).size.width ~/ 250;
      if (tilesInRow <= 0) tilesInRow = 1;
      if (tilesInRow > 5) tilesInRow = 5;
    }

    /**** TASK11 & TASK12 ****/
    return Scrollbar(
      child: GridView.count(
        primary: true,
        scrollDirection: Axis.vertical,
        padding: EdgeInsets.zero,
        crossAxisSpacing: 8,
        mainAxisSpacing: 8,
        crossAxisCount: tilesInRow,
        children: [
          ActiveFinger(FingersType.bullshit),
          ActiveFinger(FingersType.technical),
          ActiveFinger(FingersType.clarification),
          ActiveFinger(FingersType.comment),
          ActiveFinger(FingersType.question),
        ],
      ),
    );
  }
}

class ActiveFinger extends StatefulWidget {
  final FingersType type;

  ActiveFinger(this.type, {Key? key}) : super(key: key);

  @override
  _ActiveFingerState createState() => _ActiveFingerState();
}

class _ActiveFingerState extends State<ActiveFinger> {
  @override
  Widget build(BuildContext context) {
    var list = Provider.of<ActiveFingersList>(context);
    var settings = Provider.of<TF.Settings>(context);

    return Container(
      margin: EdgeInsets.all(2),
      decoration: BoxDecoration(
        border: Border.all(
          color: Theme.of(context).primaryColor,
          width: 2,
        ),
        borderRadius: BorderRadius.all(Radius.circular(8)),
        color: Colors.transparent,
      ),
      child: Material(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        clipBehavior: Clip.hardEdge,
        child: InkWell(
          onTap: () {
            list.add(Finger(widget.type, owner: settings.user, id: settings.userToken));
          },
          onLongPress: () {
            var finger = Finger(widget.type, owner: settings.user, id: settings.userToken);
            // The comment stays local, no need to wait for it.
            list.add(finger);
            finger.edit(context);
          },
          child: Padding(padding: EdgeInsets.all(6), child: FingerImage(assetsPath[widget.type]!)),
        ),
      ),
    );
  }
}
