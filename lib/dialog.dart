import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:threefingers/fingers.dart';

class DialogManager {
  static Map<String?, BuildContext> _currentDialogs = Map();

  DialogManager._();

  static void charge(String? key, BuildContext context) {
    _currentDialogs[key] = context;
  }

  static void decharge(String? key, {dynamic value}) {
    if (_currentDialogs.containsKey(key)) {
      try {
        if (value != null)
          Navigator.pop(_currentDialogs[key]!, value);
        else
          Navigator.pop(_currentDialogs[key]!);
      } catch (e) {}
      _currentDialogs.remove(key);
    }
  }
}

class SessionDialog extends StatefulWidget {
  final String title = 'Welcome!';

  SessionDialog({Key? key}) : super(key: key);

  @override
  _SessionDialogState createState() => _SessionDialogState();
}

class _SessionDialogState extends State<SessionDialog> {
  final formKey = GlobalKey<FormState>();
  bool isFromForm = false;
  String? session;
  List<String>? existingSessionsIds;

  @override
  Widget build(context) {
    String key = 'SessionDialog';
    DialogManager.charge(key, context);

    return WillPopScope(
      // Prevent the dialog from being dismissed on clicking the back navigation
      // button
      onWillPop: () async => false,
      child: SimpleDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8))),
        titlePadding: EdgeInsets.fromLTRB(24.0, 8.0, 24.0, 4.0),
        title: Text(widget.title),
        contentPadding: EdgeInsets.all(16),
        children: [
          Form(
            key: formKey,
            child: TextFormField(
              autofocus: false,
              minLines: 1,
              maxLength: 50,
              maxLengthEnforcement: MaxLengthEnforcement.enforced,
              textInputAction: TextInputAction.done,
              cursorColor: Theme.of(context).primaryColor,
              decoration: InputDecoration(
                labelText: 'Create a session',
                hintText: 'Eg: OM IT Department',
                border: OutlineInputBorder(),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ),
              validator: (value) {
                if (value!.contains('/')) return "Avoid using '/'.";
                if (existingSessionsIds?.contains(value) ?? false) return 'Session already exists!';
                return null;
              },
              onChanged: (text) {
                setState(() {
                  session = text.trim();
                  isFromForm = true;
                });
              },
            ),
          ),
          Padding(padding: EdgeInsets.all(6)),
          StreamBuilder<QuerySnapshot>(
              stream: FirebaseFirestore.instance.collection('sessions').snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData)
                  return Container(child: Center(child: CircularProgressIndicator()));
                else {
                  List<DocumentSnapshot> documentsList = snapshot.data!.docs;

                  List<DropdownMenuItem<dynamic>> documentsIdList =
                      documentsList.map((DocumentSnapshot document) {
                    return DropdownMenuItem<String>(
                      child: Text(document.id),
                      value: document.id,
                    );
                  }).toList();

                  existingSessionsIds = documentsList.map((DocumentSnapshot document) {
                    return document.id;
                  }).toList();

                  return DropdownButtonFormField(
                    autovalidateMode: AutovalidateMode.always,
                    autofocus: true,
                    decoration: InputDecoration(
                      labelText: 'Or join a session',
                      hintText: 'Choose a session',
                      border: OutlineInputBorder(),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                    ),
                    onChanged: (dynamic value) {
                      setState(() {
                        session = value.trim();
                        isFromForm = false;
                      });
                    },
                    onSaved: (dynamic text) {
                      setState(() {
                        session = text.trim();
                        isFromForm = false;
                      });
                    },
                    items: documentsIdList,
                  );
                }
              }),
          Container(
            margin: EdgeInsets.fromLTRB(0, 16, 0, 0),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(
                color: Theme.of(context).primaryColor,
                width: 2,
              ),
            ),
            child: IconButton(
              padding: EdgeInsets.zero,
              icon: Icon(Icons.check),
              onPressed: session == null
                  ? null
                  : () {
                      if (!isFromForm || formKey.currentState!.validate())
                        DialogManager.decharge(key, value: session);
                    },
            ),
          ),
        ],
      ),
    );
  }
}

class EditingDialog extends StatefulWidget {
  final TextEditingController controller;
  final String title;
  final String hintText;
  final int maxLines;
  final Finger? finger;

  EditingDialog(this.controller, this.title, this.hintText,
      {this.maxLines = 5, this.finger, Key? key})
      : super(key: key);

  @override
  _EditingDialogState createState() => _EditingDialogState();
}

class _EditingDialogState extends State<EditingDialog> {
  @override
  Widget build(context) {
    // This is ok because EditingDialog objects are always displayed one by one
    String key = widget.finger?.creationTime.toIso8601String().substring(0, 23) ?? 'EditingDialog';
    DialogManager.charge(key, context);

    return SimpleDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8))),
      titlePadding: EdgeInsets.fromLTRB(24.0, 8.0, 24.0, 4.0),
      title: Text(widget.title),
      contentPadding: EdgeInsets.all(8),
      children: [
        Scrollbar(
          child: TextField(
            autofocus: true,
            controller: widget.controller,
            cursorColor: Theme.of(context).primaryColor,
            decoration: InputDecoration(
              hintText: widget.hintText,
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Theme.of(context).primaryColor,
                ),
              ),
            ),
            minLines: 1,
            maxLines: widget.maxLines,
            onSubmitted: (_) => DialogManager.decharge(key),
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(
              color: Theme.of(context).primaryColor,
              width: 2,
            ),
          ),
          child: IconButton(
            icon: Icon(Icons.check),
            onPressed: () => DialogManager.decharge(key),
          ),
        ),
      ],
    );
  }
}

class AllowedDialog extends StatelessWidget {
  final String? title;
  final Finger? finger;
  final Function(Finger?) onQuit;

  AllowedDialog(this.title, this.finger, this.onQuit, {Key? key}) : super(key: key);

  @override
  Widget build(context) {
    DialogManager.charge(finger?.creationTime.toIso8601String().substring(0, 23), context);

    return SimpleDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8))),
      title: Text(title!),
      titlePadding: EdgeInsets.fromLTRB(24.0, 8.0, 24.0, 4.0),
      contentPadding: EdgeInsets.all(8),
      children: [
        Container(
          margin: EdgeInsets.all(2),
          decoration: BoxDecoration(
            border: Border.all(
              color: Theme.of(context).primaryColor,
              width: 2,
            ),
            borderRadius: BorderRadius.all(Radius.circular(8)),
            color: Colors.transparent,
          ),
          child: Padding(
            padding: EdgeInsets.all(6),
            child: Column(
              children: [
                Container(
                  height: 60,
                  child: finger?.image,
                ),
                if (finger?.comment?.trim().isNotEmpty ?? false)
                  Divider(
                    thickness: 2,
                  ),
                if (finger?.comment?.trim().isNotEmpty ?? false)
                  Container(
                    constraints: BoxConstraints.loose(Size.fromHeight(
                        MediaQuery.of(context).orientation == Orientation.portrait ? 300 : 80)),
                    child: Scrollbar(
                      child: SelectableText(
                        finger!.comment!,
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(
              color: Theme.of(context).primaryColor,
              width: 2,
            ),
          ),
          child: IconButton(
            icon: Icon(Icons.check),
            onPressed: () {
              onQuit(finger);
              DialogManager.decharge(finger?.creationTime.toIso8601String().substring(0, 23));
            },
          ),
        ),
      ],
    );
  }
}

class CutAllDialog extends StatelessWidget {
  CutAllDialog({Key? key}) : super(key: key);

  @override
  Widget build(context) {
    DialogManager.charge('CutAllDialog', context);

    return SimpleDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8))),
      title: Text(
        'Cut all fingers?',
        textAlign: TextAlign.center,
      ),
      titlePadding: EdgeInsets.fromLTRB(24.0, 8.0, 24.0, 4.0),
      contentPadding: EdgeInsets.all(8),
      children: [
        Row(
          children: [
            Expanded(
              child: Container(
                margin: EdgeInsets.zero,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                    color: Colors.red,
                    width: 2,
                  ),
                ),
                child: IconButton(
                  icon: Icon(
                    Icons.check,
                    color: Colors.red,
                  ),
                  onPressed: () {
                    DialogManager.decharge('CutAllDialog', value: true);
                  },
                ),
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.zero,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                    color: Theme.of(context).primaryColor,
                    width: 2,
                  ),
                ),
                child: IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    DialogManager.decharge('CutAllDialog', value: false);
                  },
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class SnackDialog {
  SnackDialog._();

  static void show(BuildContext context, String text) {
    var snackBar = SnackBar(
      duration: Duration(seconds: 3),
      content: Text(text),
      action: SnackBarAction(
        label: 'Ok',
        // By default dismisses the SnackBar
        onPressed: () {},
      ),
    );

    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
