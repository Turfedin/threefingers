// These scripts are made available when the app is served or deployed on Firebase Hosting
// If you do not serve/host your project using Firebase Hosting see https://firebase.google.com/docs/web/setup
importScripts('https://www.gstatic.com/firebasejs/8.3.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.3.1/firebase-messaging.js');
// importScripts('https://www.gstatic.com/firebasejs/8.3.1/init.js');

firebase.initializeApp({
   apiKey: "AIzaSyACNrGujNS_99MuXx1ujP8B5KlgzC7CGLo",
   authDomain: "threefingers-b767f.firebaseapp.com",
   databaseURL: "https://threefingers-b767f.firebaseio.com",
   projectId: "threefingers-b767f",
   storageBucket: "threefingers-b767f.appspot.com",
   messagingSenderId: "902329758399",
   appId: "1:902329758399:web:efbb80274ae68cc5570c0a",
   measurementId: "G-86TZN83BQ8"
});

const messaging = firebase.messaging();
 
// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START background_handler]
messaging.setBackgroundMessageHandler(function (payload) {
   const promiseChain = clients
      .matchAll({
         type: "window",
         includeUncontrolled: true
      })
      .then(windowClients => {
         for (let i = 0; i < windowClients.length; i++) {
            const windowClient = windowClients[i];
            windowClient.postMessage(payload);
         }
      })
      .then(() => {
         const title = payload.notification.title;
         const options = {
            body: payload.notification.score
         };
         return registration.showNotification(title, options);
      });
   return promiseChain;
});
self.addEventListener('notificationclick', function (event) {
   console.log('notification received: ', event)
});
// [END background_handler]