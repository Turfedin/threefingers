import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';

import 'package:threefingers/fingers.dart';
import 'package:threefingers/settings.dart' as TF;
import 'package:threefingers/notifications.dart';
import 'package:threefingers/dialog.dart';

// Whenever there is a change in the fingers list, we have to notify those that
// depend on it.
class ActiveFingersList extends ChangeNotifier {
  Finger? speakerFinger;
  String? _userToken;
  final List<Finger> _waitingList = [];
  CollectionReference? _onlineQueue;
  Timer? _timer;
  // ignore: cancel_subscriptions
  StreamSubscription? _changesStream;

  ActiveFingersList(TF.Settings settings) {
    init(settings);
  }

  int get length => _waitingList.length;

  Future<void> forceRefresh() async => notifyListeners();

  List<Finger> sort() {
    _waitingList.sort((a, b) => a.compareTo(b));
    return _waitingList;
  }

  // Return an existing Finger if it is in the list or the speaker. With really
  // bad timing, this might return null wrongly if the stream updated the list
  // before this function returns.
  Finger? find(String? creationTime, String? ownerId) {
    Finger? researchedFinger;
    if (speakerFinger?.creationTime.toIso8601String().substring(0, 23) == creationTime &&
        speakerFinger?.ownerId == ownerId)
      researchedFinger = speakerFinger;
    else
      researchedFinger = _waitingList.firstWhereOrNull((element) =>
          element.creationTime.toIso8601String().substring(0, 23) == creationTime &&
          element.ownerId == ownerId);

    return researchedFinger;
  }

  void add(Finger finger) async {
    _waitingList.add(finger);
    // Create a new document within the 'fingersQueue' collection with the data
    // of the finger.
    _onlineQueue?.add(finger.fingerData);
    // The list displayed takes immediately the new local finger into account.
    notifyListeners();
  }

  Future<void> removeAll(BuildContext context, {bool force = false, bool individual = true}) async {
    bool decision;
    if (force)
      decision = true;
    else {
      decision = await showDialog(
        context: context,
        builder: (context) => CutAllDialog(),
      );
    }
    if (decision) {
      if (individual) {
        if (speakerFinger != null && speakerFinger!.ownerId == _userToken) {
          _removeOnline(speakerFinger!);
          // Notify the owner of the finger that his/her finger was cut by
          // someone else
          if (speakerFinger!.ownerId != _userToken)
            Notifications.send(speakerFinger!.ownerId, speakerFinger!, Actions.cut);
        }
        _waitingList.forEach((element) {
          if (element.ownerId == _userToken) {
            _removeOnline(element);
          }
        });
      } else {
        if (speakerFinger != null) {
          _removeOnline(speakerFinger!);
        }
        _waitingList.forEach((element) {
          _removeOnline(element);
          if (element.ownerId != _userToken)
            Notifications.send(element.ownerId, element, Actions.cut);
        });
      }
      speakerFinger = null;
      _waitingList.clear();
    }
    notifyListeners();
  }

  void remove(Finger? finger) async {
    if (finger == null) return;

    if (speakerFinger == finger) {
      _removeOnline(finger);
      speakerFinger = null;
    } else {
      _waitingList.remove(finger);
      _removeOnline(finger);
    }
    // The list displayed takes immediately the new local finger into account.
    notifyListeners();
    // Notify the owner of the finger that his/her finger was cut by someone
    // else
    if (finger.ownerId != _userToken) Notifications.send(finger.ownerId, finger, Actions.cut);
  }

  void updateSpeaker(Finger? finger) async {
    if (finger == null) return;

    // Case where the speakerFinger is the one to be updated
    if (speakerFinger != null && finger.compareTo(speakerFinger) == 0) {
      speakerFinger = null;
      notifyListeners();
      _removeOnline(finger);
      // This is the equivalent of cuting the finger
      if (finger.ownerId != _userToken) Notifications.send(finger.ownerId, finger, Actions.cut);
    }
    // Case where we have a new speakerFinger
    else {
      _waitingList.remove(finger);
      finger.isSpeaker = true;
      Finger? tmp = speakerFinger;
      speakerFinger = finger;
      notifyListeners();

      if (tmp != null) {
        _removeOnline(tmp);
        // This is the equivalent of cuting the finger
        if (tmp.ownerId != _userToken) Notifications.send(finger.ownerId, finger, Actions.cut);
      }

      _onlineQueue
          ?.where('creationTime', isEqualTo: finger.creationTime.toIso8601String().substring(0, 23))
          .where('ownerId', isEqualTo: finger.ownerId)
          .where('type', isEqualTo: finger.type.asString())
          .limit(1)
          .get()
          .then((value) => value.docs.forEach((element) {
                element.reference.update({'isSpeaker': true});
              }));
      // Notify the owner of the finger that (s)he has the right to speak
      Notifications.send(finger.ownerId, finger, Actions.allow);
    }
  }

  Future<void> init(TF.Settings settings) async {
    _userToken = await settings.fetchId();
    DocumentReference session = settings.session;
    _onlineQueue = session.collection('fingersQueue');

    QuerySnapshot allDocuments = await _onlineQueue!.get();
    allDocuments.docs.forEach((element) {
      _onAdded(Finger.fromMap(element.data()!));
    });

    await _initStream();
  }

  Future<void> _initStream() async {
    if (_changesStream != null) _changesStream!.cancel();

    _changesStream = _onlineQueue?.snapshots().listen((event) {
      event.docChanges.forEach((element) {
        if (element.doc.data()!.length == 0) return;
        Finger tmp = Finger.fromMap(element.doc.data()!);

        switch (element.type) {
          case DocumentChangeType.removed:
            _onRemoved(tmp);
            break;
          case DocumentChangeType.added:
            _onAdded(tmp);
            break;
          case DocumentChangeType.modified:
            _onModified(tmp);
            break;
          default:
            break;
        }
        // After a first change in the list of fingers is received, all
        // changes are buffered for 2 seconds before rebuilding the layout.
        // This is to prevent the layout from being rebuilt too frequently
        // when a spike of new fingers happens.
        if (_timer == null || !_timer!.isActive)
          _timer = Timer(Duration(seconds: 1), () => notifyListeners());
      });
    });

    notifyListeners();
  }

  void _onAdded(Finger addedFinger) {
    if (addedFinger.isSpeaker) {
      // Only happens at launch, since no finger can be created with
      // the right to speak
      if (speakerFinger == null || speakerFinger!.compareTo(addedFinger) != 0)
        speakerFinger = addedFinger;
    } else {
      // Roundabout way that allows us to use the compareTo method,
      // unlike the contains method.
      try {
        _waitingList.firstWhere((element) => element.compareTo(addedFinger) == 0);
      } catch (e) {
        _waitingList.add(addedFinger);
      }
    }
  }

  void _onRemoved(Finger removedFinger) {
    if (speakerFinger != null && speakerFinger!.compareTo(removedFinger) == 0)
      speakerFinger = null;
    else
      _waitingList.removeWhere((element) => element.compareTo(removedFinger) == 0);
  }

  void _onModified(Finger modifiedFinger) {
    if (modifiedFinger.isSpeaker && ((speakerFinger?.compareTo(modifiedFinger) ?? 1) != 0)) {
      if (modifiedFinger.ownerId == _userToken) {
        // The finger changed belongs to this user, we have to fetch
        // it to keep the comment
        speakerFinger = find(modifiedFinger.creationTime.toIso8601String().substring(0, 23),
                modifiedFinger.ownerId) ??
            modifiedFinger;
        speakerFinger!.isSpeaker = true;
      } else {
        // The finger changed does not belongs to this user
        speakerFinger = modifiedFinger;
      }
      _waitingList.removeWhere((element) => element.compareTo(modifiedFinger) == 0);
    }
  }

  void _removeOnline(Finger finger) async {
    _onlineQueue
        ?.where('creationTime', isEqualTo: finger.creationTime.toIso8601String().substring(0, 23))
        .where('owner', isEqualTo: finger.owner)
        .where('type', isEqualTo: finger.type.asString())
        .limit(1)
        .get()
        .then((value) => value.docs.forEach((element) {
              FirebaseFirestore.instance
                  .runTransaction((transaction) async => transaction.delete(element.reference));
            }));
  }
}

class ActiveFingerListTile extends StatefulWidget {
  final Finger? finger;
  final bool isDeployed;

  ActiveFingerListTile(this.finger, this.isDeployed, {Key? key}) : super(key: key);

  @override
  _ActiveFingerListTileState createState() => _ActiveFingerListTileState();
}

class _ActiveFingerListTileState extends State<ActiveFingerListTile> {
  _ActiveFingerListTileState();

  void _executeAction(Actions action, ActiveFingersList list, TF.Settings settings) {
    switch (action) {
      case Actions.allow:
        // Only the facilitator can grant the right to speak
        if (settings.isFacilitator!) {
          list.updateSpeaker(widget.finger);
        }
        break;
      case Actions.cut:
        // Only the facilitator and the owner of a finger can cut it
        if (settings.isFacilitator! || settings.userToken == widget.finger!.ownerId) {
          list.remove(widget.finger);
        }
        break;
      case Actions.edit:
        // It would not make sense to edit a finger with the right to speak.
        if (list.speakerFinger != widget.finger && settings.userToken == widget.finger!.ownerId) {
          widget.finger!.edit(context);
        }
        break;
      default:
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final TF.Settings settings = Provider.of<TF.Settings>(context);
    final ActiveFingersList list = Provider.of<ActiveFingersList>(context);

    return Container(
      padding: EdgeInsets.zero,
      margin: EdgeInsets.all(3),
      decoration: BoxDecoration(
        border: Border.all(
          // Different color for the fingers the user owns.
          color: settings.userToken == widget.finger!.ownerId
              ? Theme.of(context).primaryColorDark
              : Theme.of(context).primaryColor,
          width: settings.userToken == widget.finger!.ownerId ? 2 : 1,
        ),
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      child: Material(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        clipBehavior: Clip.hardEdge,
        child: InkWell(
          // Edit comment if the user owns the finger
          onLongPress: () => _executeAction(Actions.edit, list, settings),
          onDoubleTap: () => _executeAction(Actions.allow, list, settings),
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.zero,
                padding: EdgeInsets.all(2),
                width: 50,
                height: 50,
                child: widget.finger!.image,
              ),
              if (widget.isDeployed) Padding(padding: EdgeInsets.symmetric(horizontal: 3)),
              if (widget.isDeployed)
                Expanded(
                  child: Text(
                    widget.finger!.owner,
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              if (widget.isDeployed &&
                  (settings.isFacilitator! || widget.finger!.ownerId == settings.userToken))
                PopupMenuAction(widget.finger, _executeAction),
            ],
          ),
        ),
      ),
    );
  }
}

enum Actions {
  allow,
  cut,
  edit,
}

class PopupMenuAction extends StatelessWidget {
  final List<PopupMenuItem<Actions>> actionList = [];
  final Finger? finger;
  final void Function(Actions, ActiveFingersList, TF.Settings) actionFunction;

  PopupMenuAction(this.finger, this.actionFunction);

  @override
  Widget build(BuildContext context) {
    final TF.Settings settings = Provider.of<TF.Settings>(context);
    final ActiveFingersList list = Provider.of<ActiveFingersList>(context);

    // Only the facilitator can grant the right to speak.
    if (settings.isFacilitator!) {
      actionList.add(PopupMenuItem(
        value: Actions.allow,
        child: ListTile(
          contentPadding: EdgeInsets.zero,
          title: Text('Allow'),
          trailing: Icon(Icons.play_arrow),
        ),
      ));
    }
    // Both the facilitator and the owner can cut a finger.
    if (settings.isFacilitator! || settings.userToken == finger!.ownerId) {
      actionList.add(PopupMenuItem(
        value: Actions.cut,
        child: ListTile(
          contentPadding: EdgeInsets.zero,
          title: Text('Cut'),
          trailing: Icon(Icons.content_cut),
        ),
      ));
    }
    // Only its owner can edit the comment of a finger, but not if this finger
    // has the right to speak.
    if (settings.userToken == finger!.ownerId && list.speakerFinger != finger) {
      actionList.add(PopupMenuItem(
        value: Actions.edit,
        child: ListTile(
          contentPadding: EdgeInsets.zero,
          title: Text('Edit'),
          trailing: Icon(Icons.edit),
        ),
      ));
    }

    return PopupMenuButton<Actions>(
      onSelected: (Actions selectedAction) => actionFunction(selectedAction, list, settings),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8))),
      tooltip: 'Choose an action.',
      itemBuilder: (context) => actionList,
    );
  }
}

class FingersList extends StatelessWidget {
  final bool isDeployed;

  FingersList(this.isDeployed, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.zero,
      margin: EdgeInsets.zero,
      decoration: BoxDecoration(
        border: Border.all(
          color: isDeployed ? Theme.of(context).primaryColorDark : Theme.of(context).primaryColor,
          width: 2,
        ),
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      child: Consumer<ActiveFingersList>(builder: (context, list, child) {
        var sortedlist = list.sort();
        return Column(
          children: [
            if (list.speakerFinger != null)
              SizedBox(
                height: 60,
                child: ActiveFingerListTile(list.speakerFinger, isDeployed),
              ),
            if (list.speakerFinger != null)
              Divider(
                thickness: 2,
                indent: 8,
                endIndent: 8,
              ),
            Expanded(
              child: RefreshIndicator(
                color: Theme.of(context).primaryColorDark,
                backgroundColor: Theme.of(context).primaryColor,
                displacement: 10,
                onRefresh: () => list.forceRefresh(),
                child: Scrollbar(
                  child: ListView.builder(
                    itemExtent: 60,
                    padding: EdgeInsets.zero,
                    keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
                    itemCount: list.length,
                    itemBuilder: (context, index) {
                      return ActiveFingerListTile(sortedlist[index], isDeployed);
                    },
                  ),
                ),
              ),
            ),
          ],
        );
      }),
    );
  }
}
