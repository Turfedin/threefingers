import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:provider/provider.dart';

import 'package:threefingers/fingers.dart';
import 'package:threefingers/fingersqueue.dart' as fq;
import 'package:threefingers/dialog.dart';

/*
 * Note: I am sending the title in the payload ('data') of the notification
 * because there is curently an unresolved issue that deletes all the fields of
 * the 'notification' when using the callback onResume.
 * Issue: https://github.com/FirebaseExtended/flutterfire/issues/2012
 */

class Notifications {
  static late BuildContext _context;
  static late fq.ActiveFingersList list;
  static final String serverToken =
      'AAAA0hcLcr8:APA91bGyJJtt-1F2AKgGX0HUNtb6Sz6FcFa7S-r_kdsDlzmsOfPV-kuvk-FxALGdXxQ-dIiO0hRvDAoJlagfwM_s5dzgNbytYPXfdq9yuu9e370bUFYf2XbB1cyTghrwcEzhpqjrsZcj';

  Notifications._();

  static void init(BuildContext context) {
    _context = context;
    list = Provider.of<fq.ActiveFingersList>(context);
  }

  static void send(String target, Finger finger, fq.Actions action) async {
    String title;
    if (action == fq.Actions.allow)
      title = 'Your turn to speak!';
    else if (action == fq.Actions.cut)
      title = 'A finger belonging to you has been cut!';
    else
      title = 'Unexpected notification';

    await http.post(
      Uri.parse('https://fcm.googleapis.com/fcm/send'),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Authorization': 'key=$serverToken',
      },
      body: jsonEncode(
        <String, dynamic>{
          'notification': <String, dynamic>{'title': title},
          'priority': 'high',
          'data': <String, dynamic>{
            'click_action': 'FLUTTER_NOTIFICATION_CLICK',
            'actionType': action.toString(),
            'priority': finger.priority,
            'creationTime': finger.creationTime.toIso8601String().substring(0, 23),
            'ownerId': finger.ownerId,
            'title': title
          },
          'to': target,
        },
      ),
    );
  }

  static void configureFcm() async {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      _handleNotification(message);
    });

    FirebaseMessaging.onBackgroundMessage((RemoteMessage message) async {
      return _handleNotification(message);
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      _handleNotification(message);
    });
  }

  static void _handleNotification(RemoteMessage message) {
    // If there was already an openend dalog concerning that finger, dismiss it
    DialogManager.decharge(message.data['creationTime']);

    if (message.data['actionType'] == fq.Actions.cut.toString()) {
      /**** TASK7 ****/
      SnackDialog.show(
          _context, message.notification?.title ?? 'A finger belonging to you has been cut!');
    } else {
      if (message.data['actionType'] == fq.Actions.allow.toString()) {
        _showAllowed(message);
      }
    }
  }

  static _showAllowed(RemoteMessage message) async {
    await showDialog(
        context: _context,
        builder: (_context) {
          return AllowedDialog(message.data['title'],
              list.find(message.data['creationTime'], message.data['ownerId']), list.remove);
        });
  }
}
