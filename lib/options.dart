import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'package:threefingers/settings.dart' as TF;
import 'package:threefingers/fingersqueue.dart';
import 'package:threefingers/dialog.dart';

class OptionsMenu extends StatelessWidget {
  OptionsMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TF.Settings settings = Provider.of<TF.Settings>(context);
    final ActiveFingersList list = Provider.of<ActiveFingersList>(context);

    var controller = TextEditingController(text: settings.user);
    controller.addListener(() {
      _saveTextFromEdit(controller, settings);
    });

    return Drawer(
      child: Scrollbar(
        child: ListView(
          padding: EdgeInsets.zero,
          keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
          children: <Widget>[
            // SafeArea to take into account notches and other physical
            // differences of screens
            SafeArea(
              child: Container(
                height: 56,
                child: DrawerHeader(
                  margin: EdgeInsets.zero,
                  padding: EdgeInsets.all(16),
                  child: Text(
                    "Options",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ),
            ),
            SwitchListTile(
              activeColor: Theme.of(context).primaryColor,
              title: Text("Enable dark theme"),
              value: settings.isDarkTheme ??
                  MediaQuery.platformBrightnessOf(context) == Brightness.dark,
              onChanged: (bool value) {
                settings.theme = value;
                // Close the drawer
                Navigator.pop(context);
              },
            ),
            StreamBuilder<DocumentSnapshot>(
                stream: settings.session.snapshots(),
                builder: (context, snapshot) {
                  Widget? title, subtitle, trailing;

                  if (settings.isFacilitator!) {
                    title = Text('Surrender facilitation');
                    subtitle = Text('You are the facilitator');
                    trailing = Padding(
                      padding: EdgeInsets.fromLTRB(0, 0, 17, 0),
                      child: Icon(Icons.cancel),
                    );
                  } else {
                    try {
                      if (snapshot.data!['facilitator'] == '') {
                        title = Text('Request facilitation rights');
                        subtitle = Text('There is no facilitator');
                        trailing = Padding(
                          padding: EdgeInsets.fromLTRB(0, 0, 17, 0),
                          child: Icon(Icons.gavel),
                        );
                      } else {
                        title = Text('There is a facilitator');
                        subtitle = null;
                        trailing = Padding(
                          padding: EdgeInsets.fromLTRB(0, 0, 17, 0),
                          child: Icon(Icons.check),
                        );
                      }
                    } catch (e) {
                      title = Text('Waiting...');
                      subtitle = Text('Maybe you should leave');
                      trailing = Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 11, 0),
                        child: CircularProgressIndicator(),
                      );
                    }
                  }

                  return ListTile(
                    title: title,
                    subtitle: subtitle,
                    trailing: trailing,
                    onTap: () async {
                      await settings.updateFacilitator();
                      Navigator.pop(context);
                    },
                  );
                }),
            ListTile(
              title: Text("Edit username"),
              subtitle: Text(settings.user!),
              trailing: Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 17, 0),
                child: Icon(Icons.edit),
              ),
              onTap: () async {
                await showDialog(
                  context: context,
                  builder: (context) => EditingDialog(
                    controller,
                    'Edit your username',
                    'Eg: Simon-Liège',
                    maxLines: 1,
                  ),
                );
                // Close the drawer
                Navigator.pop(context);
                settings.save();
              },
            ),
            ListTile(
              title: Text("Change session"),
              subtitle: Text(settings.session.id),
              trailing: Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 17, 0),
                child: Icon(Icons.edit),
              ),
              onTap: () async {
                // Close the drawer
                Navigator.pop(context);

                // When the facilitator leaves a session, (s)he lose the
                // facilitations rights
                if (settings.isFacilitator!) settings.updateFacilitator();

                var previousSession = settings.session.id;
                await settings.initSession(context);
                if (previousSession != settings.session.id) {
                  await list.removeAll(context, force: true);
                  await list.init(settings);
                }
              },
            ),
            if (settings.isFacilitator!)
              ListTile(
                title: Text(
                  'Reinitialize counts',
                  style: TextStyle(color: Colors.orange[700]),
                ),
                trailing: Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 17, 0),
                  child: Icon(
                    Icons.refresh,
                    color: Colors.orange[700],
                  ),
                ),
                onTap: () async {
                  // Close the drawer
                  Navigator.pop(context);
                  await settings.resetCounts();
                },
              ),
            if (settings.isFacilitator!)
              ListTile(
                title: Text(
                  'Delete session & quit',
                  style: TextStyle(color: Colors.red),
                ),
                trailing: Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 17, 0),
                  child: Icon(
                    Icons.delete,
                    color: Colors.red,
                  ),
                ),
                onTap: () async {
                  await list.removeAll(context, force: true, individual: false);
                  await settings.resetCounts();
                  settings.session.collection('passiveFingers').get().then((value) {
                    for (DocumentSnapshot document in value.docs) document.reference.delete();
                  });
                  await settings.updateFacilitator();
                  await settings.session.delete();

                  SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                },
              ),
            /**** TASK13 ****/
            GestureDetector(
              onTap: () {
                // Close the drawer
                Navigator.pop(context);
              },
              child: AboutListTile(
                child: Text("About this application"),
                applicationName: 'Three Fingers',
                aboutBoxChildren: [
                  Text('This application was created by Simon Petit for the' +
                      ' course INFO2051 of the University of Liège.'),
                  Text('\nFingers icons made by Freepik from www.flaticon.com' +
                      ' and modified by Simon Petit.')
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _saveTextFromEdit(TextEditingController controller, TF.Settings settings) {
    settings.user = controller.text;
  }
}
