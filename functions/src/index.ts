import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp();

const fcm = admin.messaging();

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });

// Triggers everytmes a document in the collection fingersQueue is updated.
export const notifySpeaker = functions.firestore
   .document('fingersQueue/{docId}')
   .onUpdate((change, _) => {
      const updatedDoc = change.after.data();

      if (updatedDoc.isSpeaker == false) { return; }

      const token = updatedDoc.ownerId;

      const payload: admin.messaging.MessagingPayload = {
         notification: {
            title: 'Your turn to speak!',
            click_action: 'FLUTTER_NOTIFICATION_CLICK'
         },
         data: {
            creationTime: updatedDoc.creationTime,
         }
      };

      return fcm.sendToDevice(token, payload);
   });
