import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:threefingers/settings.dart' as TF;
import 'package:threefingers/options.dart';
import 'package:threefingers/notifications.dart';
import 'package:threefingers/fingersqueue.dart';
import 'package:threefingers/activefingers.dart';
import 'package:threefingers/passivefingers.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  /**** TASK2 ****/
  runApp(App());
}

// Widget whose sole purpose is to host the initialization of Firebase
class App extends StatelessWidget {
  // Create the initialization Future outside of `build`:
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      // Initialize FlutterFire:
      future: _initialization,
      builder: (context, snapshot) {
        // Check for errors
        if (snapshot.hasError) {
          exit(1);
        }

        // Once complete, show your application
        if (snapshot.connectionState == ConnectionState.done) {
          // When the settings are changed, we need to reconstruct the whole app
          return ChangeNotifierProvider(
            create: (context) => TF.Settings(),
            child: ThreeFingersApp(),
          );
        }

        // Otherwise, show something whilst waiting for initialization to complete
        return Container(child: Center(child: CircularProgressIndicator()));
      },
    );
  }
}

class ThreeFingersApp extends StatelessWidget {
  final String title = "Three Fingers";
  // This widget is the root of your application.

  ThreeFingersApp({Key? key}) : super(key: key) {
    Notifications.configureFcm();
  }

  @override
  Widget build(BuildContext context) {
    final TF.Settings settings = Provider.of<TF.Settings>(context);

    return MaterialApp(
      title: this.title,
      theme: TF.lightTheme,
      darkTheme: TF.darkTheme,
      themeMode: settings.themeMode,
      home: MainScreen(this.title, settings),
    );
  }
}

class MainScreen extends StatefulWidget {
  final String title;
  final TF.Settings settings;

  MainScreen(this.title, this.settings, {Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  bool _isReady = false;
  bool _isListDeployed = false;
  Orientation? previousOrientation;

  _MainScreenState();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      widget.settings.initSession(context).then((_) {
        setState(() {
          _isReady = true;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!_isReady) {
      return Container(
        color: Colors.transparent,
      );
    } else {
      var list = ActiveFingersList(widget.settings);

      return ChangeNotifierProvider(
        create: (context) => list,
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          drawer: OptionsMenu(),
          appBar: AppBar(
            /**** TASK3 ****/
            title: Text(widget.settings.session.id),
          ),
          body: OrientationBuilder(builder: (context, orientation) {
            // Allows us to summon SnackBars outside of a build method.
            Notifications.init(context);

            if (previousOrientation != orientation) {
              if (orientation == Orientation.landscape)
                _isListDeployed = true;
              else
                _isListDeployed = false;
            }
            previousOrientation = orientation;

            return GestureDetector(
              /**** TASK4 ****/
              // Otherwise, Flutter merges vertical and horizontal behaviours
              onVerticalDragStart: (details) {},
              onHorizontalDragStart: (details) {
                setState(() {
                  _isListDeployed = !_isListDeployed;
                });
              },
              child: Row(
                children: <Widget>[
                  Column(
                    children: [
                      Expanded(
                        flex: 10,
                        child: Container(
                          width: _isListDeployed ? 200 : 64,
                          alignment: Alignment.topCenter,
                          margin: widget.settings.isFacilitator!
                              ? EdgeInsets.fromLTRB(8, 8, 8, 2)
                              : EdgeInsets.all(8),
                          child: FingersList(_isListDeployed),
                        ),
                      ),
                      /**** TASK5 ****/
                      if (widget.settings.isFacilitator!)
                        Expanded(
                          flex: orientation == Orientation.portrait ? 1 : 2,
                          child: Container(
                            margin: EdgeInsets.fromLTRB(8, 2, 8, 8),
                            padding: EdgeInsets.zero,
                            child: Material(
                              borderRadius: BorderRadius.all(Radius.circular(8)),
                              clipBehavior: Clip.hardEdge,
                              child: InkWell(
                                onTap: () async => await list.removeAll(context, individual: false),
                                child: Container(
                                  width: _isListDeployed ? 200 : 64,
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.zero,
                                  margin: EdgeInsets.zero,
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Colors.red,
                                      width: 2,
                                    ),
                                    borderRadius: BorderRadius.all(Radius.circular(8)),
                                  ),
                                  /**** TASK6 ****/
                                  child: Icon(
                                    Icons.delete,
                                    color: Colors.red,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                    ],
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.fromLTRB(0, 6, 6, 6),
                      padding: EdgeInsets.zero,
                      child: Column(
                        children: [
                          Flexible(
                            flex: orientation == Orientation.portrait ? 2 : 1,
                            child: ActiveFingers(_isListDeployed),
                          ),
                          Divider(
                            color: Theme.of(context).primaryColor,
                            thickness: 1,
                            indent: 8,
                            endIndent: 8,
                          ),
                          Flexible(
                            flex: 1,
                            child: PassiveFingers(),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
          }),
        ),
      );
    }
  }
}
